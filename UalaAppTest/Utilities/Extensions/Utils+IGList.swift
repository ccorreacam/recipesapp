//
//  Utils+IGList.swift
//  UalaAppTest
//
//  Created by Christian Correa on 15/11/20.
//

import Foundation
import IGListKit

public extension ListCollectionContext {
    
    func dequeueReusableCell<T>(with cellType: T.Type, for sectionController: ListSectionController, at index: Int, bundle: Bundle? = nil) -> T where T:UICollectionViewCell {
        let bundle = bundle ?? bundleForXib(type: cellType)
        let nibName = String(describing: cellType)
        guard let cell = dequeueReusableCell(withNibName: nibName, bundle: bundle, for: sectionController, at: index) as? T else {
            
            return T()
        }
        return cell
    }
    
    func dequeueReusableSupplementaryView<T: UICollectionReusableView>(with viewType: T.Type, ofKind elementKind: String, for sectionController: ListSectionController, at index: Int, bundle: Bundle? = nil) -> T {
        let bundle = bundle ?? bundleForXib(type: viewType)
        let nibName = String(describing: viewType)
        guard let view = dequeueReusableSupplementaryView(ofKind: elementKind, for: sectionController, nibName: nibName, bundle: bundle, at: index) as? T else {
            return T()
        }
        return view
    }
}

extension ListSectionController {
    
    public func reuse<T: UICollectionViewCell>(at index: Int, bundle: Bundle? = nil) -> T {
        guard let context = collectionContext else { return T() }
        let bundle = bundle ?? bundleForXib(type: T.self)
        let cell: T = context.dequeueReusableCell(with: T.self, for: self, at: index, bundle: bundle)
        return cell
    }
}

public func bundleFor(root object: AnyClass, name: String) -> Bundle? {
    guard let path = Bundle(for: object).path(forResource: name, ofType: "bundle"), let bundle = Bundle(path: path) else { return nil }
    return bundle
}

public func bundleForXib<T: NSObject>(type: T.Type) -> Bundle {
    let defaultBundle = Bundle(for: T.classForCoder())
    let name = String(describing: type)
    
    if let podName = String(reflecting: type).split(separator: ".").first,
        let resourcesBundle = bundleFor(root: T.classForCoder(), name: podName + "Resources"),
        resourcesBundle.has(xib: name) { return resourcesBundle }
    
    if defaultBundle.has(xib: name) { return defaultBundle }
    
    if !Bundle.main.has(xib: name) {
        print("The xib named: " + name + " isn't in the resources bundle")
    }
    return .main
}


public extension Bundle {
    func has(xib: String) -> Bool {
        return path(forResource: xib, ofType: "nib") != nil
    }
}
