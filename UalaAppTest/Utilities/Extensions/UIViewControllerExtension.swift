//
//  UIViewControllerExtension.swift
//  UalaAppTest
//
//  Created by Mobile Dev on 15/11/20.
//

import Foundation
import UIKit

extension UIViewController {
    
    /// Creates an instance of the view controller from the storyboard.
    ///
    /// - Parameters:
    ///   - name: The name of the storyboard where the view controller is defined.
    ///   - bundle: The bundle where the storyboard belongs. Default vaule is `nil` for which
    ///             the main bundle is used.
    ///   - id: The storyboard identifier for the view controller. If `nil` uses the view controller's type as the identifier. Default value is `nil`.
    /// - Returns: An instance of the view controller or `nil` if the view controller with the identifier does not exists in the storyboard.
    class func fromStoryboard(_ name: String, in bundle: Bundle? = nil, withIdentifier id: String? = nil) -> Self? {
        return fromStoryboard(UIStoryboard(name: name, bundle: bundle), withIdentifier: id)
    }
    
    /// Creates an instance of the view controller from the storyboard.
    ///
    /// - Parameters:
    ///   - storyboard: The storyboard where the view controller is defined.
    ///   - id: The storyboard identifier for the view controller. If `nil` uses the view controller's type as the identifier. Default value is `nil`.
    /// - Returns: An instance of the view controller or `nil` if the view controller with the identifier does not exists in the storyboard.
    class func fromStoryboard(_ storyboard: UIStoryboard, withIdentifier id: String? = nil) -> Self? {
        return fromStoryboard(storyboard, withIdentifier: id, as: self)
    }
    
    // MARK: Private
    
    private class func fromStoryboard<T>(_ storyboard: UIStoryboard, withIdentifier id: String? = nil, as type: T.Type) -> T? {
        return  storyboard.instantiateViewController(withIdentifier: id ?? "\(type)") as? T
    }
}
