//
//  UIViewExtension.swift
//  UalaAppTest
//
//  Created by Christian Correa on 15/11/20.
//

import Foundation
import UIKit

public struct RoundedCorners {
    
    public let radius: CGFloat
    public let corners: UIRectCorner
    
    public init(radius: CGFloat, corners: UIRectCorner) {
        self.radius = radius
        self.corners = corners
    }
}

public extension UIView {
    func setup(roundedCorners: RoundedCorners) {
        setupRoundedCorners(radius: roundedCorners.radius, corners: roundedCorners.corners)
    }
    
    func setupRoundedCorners(radius: CGFloat, corners: UIRectCorner) {
        layer.masksToBounds = true
        if #available(iOS 11, *) {
            var cornerMask = CACornerMask()
            if(corners.contains(.topLeft)) {
                cornerMask.insert(.layerMinXMinYCorner)
            }
            if(corners.contains(.topRight)) {
                cornerMask.insert(.layerMaxXMinYCorner)
            }
            if(corners.contains(.bottomLeft)) {
                cornerMask.insert(.layerMinXMaxYCorner)
            }
            if(corners.contains(.bottomRight)) {
                cornerMask.insert(.layerMaxXMaxYCorner)
            }
            layer.cornerRadius = radius
            layer.maskedCorners = cornerMask
        } else {
            let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
            let mask = CAShapeLayer()
            mask.path = path.cgPath
            layer.mask = mask
        }
        
        layer.shouldRasterize = true
        layer.rasterizationScale = UIScreen.main.scale
    }
}
