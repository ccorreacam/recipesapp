//
//  Then.swift
//  UalaAppTest
//
//  Created by Christian Correa on 15/11/20.
//

import Foundation

@objc public protocol Then {}
extension Then {
    
    @discardableResult
    public func then(_ block: (Self) -> Void) -> Self {
        block(self)
        return self
    }

}

extension NSObject: Then {}
