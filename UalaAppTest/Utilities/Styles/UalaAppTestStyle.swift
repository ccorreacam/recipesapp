//
//  UalaAppTestStyle.swift
//  UalaAppTest
//
//  Created by Christian Correa on 15/11/20.
//

import Foundation
import UIKit

public final class UalaAppTestStyle {
    
    // MARK: - Color palette
    public static let colors = PrimaryColor()    
}

public struct PrimaryColor {
    
    public let content: UIColor = UIColor(red: 112, green: 105, blue: 103, alpha: 1.0)
    public let disable: UIColor = UIColor(red: 184, green: 180, blue: 180, alpha: 1.0)
    public let divider: UIColor = UIColor(red: 184, green: 180, blue: 180, alpha: 1.0)
    public let shadow: UIColor = UIColor(red: 128, green: 98, blue: 96, alpha: 0.36)
}
