//
//  CollectionDataSource.swift
//  UalaAppTest
//
//  Created by Christian Correa on 15/11/20.
//

import UIKit
import IGListKit
import RxSwift
import RxCocoa

//MARK: CollectionDataSourceDelegate
protocol CollectionDataSourceDelegate: AnyObject {
    var sectionManagers: [CollectionDataSourceManager] { get }
}

//MARK: CollectionDataSourceManager
protocol CollectionDataSourceManager {
    var diffables: Observable<[ListDiffable]> { get }
    func sectionController(for object: ListDiffable) -> ListSectionController?
}

//MARK: CollectionDataSource
class CollectionDataSource: NSObject {
    private var items: [ListDiffable] = []
    private let updatedSubject = ReplaySubject<Void>.create(bufferSize: 1)
    private weak var delegate: CollectionDataSourceDelegate?
    private let disposeBag = DisposeBag()

    var updateDriver: Driver<Void> {
        return updatedSubject.asDriver(onErrorDriveWith: .never())
    }

    init(delegate: CollectionDataSourceDelegate) {
        super.init()
        self.delegate = delegate
        bind()
    }
}

//MARK: extension CollectionDataSource
extension CollectionDataSource: ListAdapterDataSource {
    func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
        return items
    }

    func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        guard let managers = delegate?.sectionManagers,
            let diffable = object as? ListDiffable
            else { return ListSectionController() }
        
        for manager in managers {
            if let sectionController = manager.sectionController(for: diffable) {
                return sectionController
            }
        }

        return ListSectionController()
    }

    func emptyView(for listAdapter: ListAdapter) -> UIView? {
        return nil
    }

}

//MARK: extension CollectionDataSource
private extension CollectionDataSource {
    func bind() {
        guard let managers = delegate?.sectionManagers else { return }
        let observables: [Observable<[ListDiffable]>] = managers.compactMap { $0.diffables }
        let sectionsObservable: Observable<[ListDiffable]> = combine(signals: observables) {
            return $0 + $1
        }

        sectionsObservable
            .do(onNext: { [weak self] items in
                self?.items = items
            })
            .map{ _ in }
            .asDriver(onErrorDriveWith: .never())
            .drive(updatedSubject)
            .disposed(by: disposeBag)
    }
    
    func combine<T>(signals: [Observable<T>], combine: @escaping (T, T) -> T ) -> Observable<T> {
        assert (signals.count >= 1)
        guard signals.count >= 2 else { return signals[0] }
        
        var comb = Observable<T>.combineLatest(signals[0], signals[1]) { combine($0, $1) }
        for i in 2..<signals.count {
            comb = Observable<T>.combineLatest(comb, signals[i]) { combine($0, $1) }
        }
        return comb
    }
}
