//
//  ApiManager.swift
//  UalaAppTest
//
//  Created by Christian Correa on 15/11/20.
//

import Foundation
import RxSwift
import RxCocoa
import SwiftyJSON

enum RequestError: Error {
    case unknownError
    case connectionError
    case authError
    case noResponseFound
    case noDataFound
    case error(message: String)
    
    var errorDescription: String? {
        switch self {
        case .unknownError:
            return "An unexpected error occurred, please try again."
        case .authError, .noResponseFound:
            return "The server was not accessed."
        case .noDataFound:
            return "No response data were obtained."
        case .connectionError:
            return "There is no internet connection."
        case .error(let message):
            return NSLocalizedString(message, comment: "")
        }
    }
}

enum HTTPMethod: String {
    case get     = "GET"
}

class APIManager {
    
    static let baseUrl = "https://www.themealdb.com/api/json/v1/1/"
    
    typealias RequestParams = [String:Any]
    
    enum ApiResponse {
        case success(JSON)
        case failure(RequestError)
    }
    
    static func executeRequest(url: String, method: HTTPMethod?, parameters: RequestParams?, completion: @escaping (ApiResponse) -> Void) {
        
        let header = ["Content-Type": "application/x-www-form-urlencoded"]
        guard let url = URL(string: baseUrl + url) else {
            completion(ApiResponse.failure(.unknownError))
            return
        }
        
        var urlRequest = URLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10)
        urlRequest.allHTTPHeaderFields = header
        urlRequest.httpMethod = method?.rawValue
        if let parameters = parameters {
            let parameterData = parameters.reduce("") { (result, param) -> String in
                return result + "&\(param.key)=\(param.value as! String)"
            }.data(using: .utf8)
            urlRequest.httpBody = parameterData
        }
        URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            if let error = error {
                print(error)
                completion(ApiResponse.failure(.connectionError))
            }else if let data = data ,let responseCode = response as? HTTPURLResponse {
                do {
                    let responseJson = try JSON(data: data)
                    DispatchQueue.main.async {
                        switch responseCode.statusCode {
                        case 200:
                        completion(ApiResponse.success(responseJson))
                        case 400...499:
                        completion(ApiResponse.failure(.authError))
                        case 500...599:
                        completion(ApiResponse.failure(.noResponseFound))
                        default:
                            completion(ApiResponse.failure(.unknownError))
                            break
                        }
                    }
                }
                catch let parseJSONError {
                    DispatchQueue.main.async {
                        completion(ApiResponse.failure(.unknownError))
                    }
                }
            }
        }.resume()
    }
}

class RequestHandler {
    static func handleResponse<T: Decodable>(response: JSON, model: T.Type, completionHandler: @escaping(T?, Error?) -> Void) {
        if let data = response.dictionaryObject {
            do {
                let dataInfo: Data = try JSONSerialization.data(withJSONObject: data)
                let decoder: JSONDecoder = JSONDecoder()
                decoder.keyDecodingStrategy = JSONDecoder.KeyDecodingStrategy.useDefaultKeys
                let results: T = try decoder.decode(model.self,
                                                    from: dataInfo)
                completionHandler(results, nil)
            } catch _ as NSError {
                completionHandler(nil, RequestError.noDataFound)
            }
        } else {
            completionHandler(nil, RequestError.noResponseFound)
        }
    }
}
