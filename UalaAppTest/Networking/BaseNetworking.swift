//
//  BaseNetworking.swift
//  UalaAppTest
//
//  Created by Christian Correa on 15/11/20.
//

import Foundation
import SwiftyJSON

class BaseNetworking {
    func executeRequest<T: Decodable>(url: String, params: APIManager.RequestParams? = nil, method: HTTPMethod? = .get, model: T.Type, completionHandler: @escaping(T?, Error? ) -> Void) {
        APIManager.executeRequest(url: url, method: method, parameters: params, completion: { (result) in
            switch result {
            case .success(let returnJson) :
                RequestHandler.handleResponse(response: returnJson, model: model) { (data, error) in
                    if let data = data {
                        completionHandler(data, nil)
                    } else if let error = error {
                        completionHandler(nil, error)
                    }
                 }
            case .failure(let failure) :
                switch failure {
                case .connectionError:
                    completionHandler(nil, RequestError.connectionError)
                case .authError:
                    completionHandler(nil, RequestError.authError)
                default:
                    completionHandler(nil, RequestError.unknownError)
                }
            }
        })
    }
}
