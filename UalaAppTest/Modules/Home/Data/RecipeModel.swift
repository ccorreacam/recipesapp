//
//  Model.swift
//  UalaAppTest
//
//  Created by Christian Correa on 15/11/20.
//

import Foundation

struct Recipes: Decodable {
    let meals: [RecipeModel]?
    
    private enum CodingKeys: String, CodingKey {
        case meals
    }
}

struct RecipeModel: Decodable {
    let picture: String
    let title: String
    let instructions: String
    let category: String
    let id: String
    let strIngredient1: String
    let strIngredient2: String
    let strIngredient3: String
    let strIngredient4: String
    let strIngredient5: String
    let strIngredient6: String
    let strIngredient7: String
    let strIngredient8: String
    let strIngredient9: String
    let strIngredient10: String
    let strIngredient11: String
    let strIngredient12: String
    let strIngredient13: String

    
    private enum CodingKeys: String, CodingKey {
        case strIngredient1, strIngredient2, strIngredient3, strIngredient4, strIngredient5, strIngredient6, strIngredient7
        case strIngredient8, strIngredient9, strIngredient10, strIngredient11, strIngredient12, strIngredient13
        case picture = "strMealThumb"
        case title = "strMeal"
        case category = "strCategory"
        case instructions = "strInstructions"
        case id = "idMeal"
     }
    
    var completeInstructionsWithIngrediens: String {
        let ingredients = "\n\n Ingredients: \n  \(strIngredient1)\n  \(strIngredient2)\n  \(strIngredient3)\n  \(strIngredient4)\n  \(strIngredient5)\n  \(strIngredient6)\n  \(strIngredient7)\n  \(strIngredient8)\n  \(strIngredient9)\n  \(strIngredient10)\n  \(strIngredient11)\n  \(strIngredient12)\n  \(strIngredient13)"
        
        return "\(instructions)\(ingredients)"
    }
}
