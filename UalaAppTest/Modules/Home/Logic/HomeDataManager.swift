//
//  DataManager.swift
//  UalaAppTest
//
//  Created by Christian Correa on 15/11/20.
//

import Foundation
import RxSwift

protocol HomeDataMagerProtocol {
    func getHomeRecipes(recipeToSearch: String) -> Single<Recipes?>
    func getRecomendedRecipe() -> Single<Recipes?>
}

class HomeDataManager: BaseNetworking {}

extension HomeDataManager: HomeDataMagerProtocol {
    func getHomeRecipes(recipeToSearch: String) -> Single<Recipes?> {
        return Single.create { [weak self] single in
            let disposables = Disposables.create()
            guard let self = self else {
                return disposables
            }
            let url = "search.php?s=\(recipeToSearch)"
            
            self.executeRequest(url: url, model: Recipes.self) { (data, error) in
                if let data = data {
                    single(.success(data))
                } else if let error = error {
                    single(.error(error))
                }
            }
            return disposables
        }
    }
    
    func getRecomendedRecipe() -> Single<Recipes?> {
        return Single.create { [weak self] single in
            let disposables = Disposables.create()
            guard let self = self else {
                return disposables
            }
            let url = "random.php"
            
            self.executeRequest(url: url, model: Recipes.self) { (data, error) in
                if let data = data {
                    single(.success(data))
                } else if let error = error {
                    single(.error(error))
                }
            }
            return disposables
        }
    }
}
