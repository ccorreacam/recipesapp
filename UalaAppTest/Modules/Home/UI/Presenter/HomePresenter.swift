//
//  Presenter.swift
//  UalaAppTest
//
//  Created by Christian Correa on 15/11/20.
//

import Foundation
import RxSwift
import RxCocoa

protocol HomePresenterProtocol: CollectionDataSourceDelegate {
    var recomendedRecipeUrl: Driver<RecipeModel?> { get }
    func setView(view: HomeViewControllerProtocolInput)
    func didLoad()
    func showRecomendedRecipe(recipe: RecipeModel?)
}

protocol HomePresenterDataManagerProtocol {
    func showError(message: String)
}

class HomePresenter {
    
    var dataManager: HomeDataMagerProtocol?
    var view: HomeViewControllerProtocolInput?
    var sectionManagers: [CollectionDataSourceManager] = []

    // MARK: Subjects
    private let disposeBag = DisposeBag()
    private let refreshSubject = PublishSubject<Void>()
    private let recipeSubject = PublishSubject<[RecipeModel]?>()
    private let recomendedRecipeSubject = PublishSubject<RecipeModel?>()

    var search = ""
    
    init() {
        self.dataManager = HomeDataManager()
    }
}

extension HomePresenter: HomePresenterProtocol {
    var recomendedRecipeUrl: Driver<RecipeModel?> {
        return recomendedRecipeSubject.asDriver(onErrorJustReturn: nil)
    }

    func setView(view: HomeViewControllerProtocolInput) {
        self.view = view
        view.searchTextDriver
            .drive(onNext: { [weak self] searchText in
                self?.search(recipeToSearch: searchText)
            })
            .disposed(by: view.disposeBag)
    }
    
    func didLoad() {
        let recipes = RecipesManager(delegate: self)
        
        bindRefreshSubject()
        bindItemsSubject(couponManager: recipes)
        sectionManagers = [recipes]
        
        refreshSubject.onNext(())
        addTimerToRecomendenRecipe()
        requestRecomendedRecipe()
    }
    
    func showRecomendedRecipe(recipe: RecipeModel?) {
        guard let recipeID = recipe?.id else { return }
        view?.presentWithID(recipeID: recipeID)
    }
    
    func search(recipeToSearch: String) {
        search = recipeToSearch
        refreshSubject.onNext(())
    }
    
    func addTimerToRecomendenRecipe() {
        Timer.scheduledTimer(withTimeInterval: 30.0, repeats: true) { [weak self] timer in
            guard let self = self else { return }
            self.requestRecomendedRecipe()
        }
    }
}

// MARK: - Privates
private extension HomePresenter {
    
    // MARK: RxBinds
    func bindRefreshSubject() {
        refreshSubject
            .bind(onNext: requestCoupons)
            .disposed(by: disposeBag)
    }
    
    func bindItemsSubject(couponManager: RecipesManager) {
        recipeSubject
            .bind(onNext: { recipesData in
                guard let recipes = recipesData else { return }
                couponManager.items = recipes
            })
            .disposed(by: disposeBag)
    }
    
    // MARK: Requests
    func requestCoupons() {
        dataManager?
            .getHomeRecipes(recipeToSearch: search)
            .asObservable()
            .subscribe(onNext: { [weak self] recipes in
                self?.recipeSubject.onNext(recipes?.meals?.shuffled())
            }, onError: { [weak self] error in
                self?.showError(message: error.localizedDescription)
            })
            .disposed(by: disposeBag)
    }
    
    func requestRecomendedRecipe() {
        dataManager?
            .getRecomendedRecipe()
            .asObservable()
            .subscribe(onNext: { [weak self] recipes in
                guard let recipe = recipes?.meals?.first else { return }
                self?.recomendedRecipeSubject.onNext(recipe)
            }, onError: { [weak self] error in
                self?.showError(message: error.localizedDescription)
                })
            .disposed(by: disposeBag)
    }
}

extension HomePresenter: HomePresenterDataManagerProtocol {
    func showError(message: String) {
        view?.showError(message: message)
    }
}

extension HomePresenter: RecipesManagerDelegate {
    func didSelect(item: RecipeModel) {
        view?.presentWithID(recipeID: item.id)
    }
}
