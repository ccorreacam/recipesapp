//
//  SplashViewController.swift
//  UalaAppTest
//
//  Created by Christian Correa on 15/11/20.
//

import UIKit
import IGListKit
import RxSwift
import RxCocoa
import SDWebImage

protocol HomeViewControllerProtocolInput {
    var disposeBag: DisposeBag { get }
    var navigationController: UINavigationController? { get }
    var searchTextDriver: Driver<String> { get }
    func showError(message: String)
    func presentWithID(recipeID: String)
}

class HomeViewController: UIViewController {
    
    @IBOutlet private weak var collectionView: UICollectionView!{
        didSet {
            collectionView.backgroundColor = .clear
        }
    }
    @IBOutlet weak var recomendedRecipeContainer: UIView! {
        didSet {
            recomendedRecipeContainer.setup(roundedCorners: RoundedCorners(radius: 15, corners: .allCorners))
        }
    }
    @IBOutlet private weak var recomendedRecipeImage: UIImageView!
    @IBOutlet private weak var recomendedRecipeButton: UIButton! {
        didSet {
            recomendedRecipeButton.rx.tap.asObservable()
            .throttle(.milliseconds(250), latest: false, scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] () in
                self?.presenter.showRecomendedRecipe(recipe: self?.recomendedRecipe)
            })
            .disposed(by: disposeBag)
        }
    }
    var searchController: UISearchController!

    private let searchTextSubject = PublishSubject<String>()
    private var dataSource: CollectionDataSource?
    
    var disposeBag = DisposeBag()
    let presenter: HomePresenterProtocol
    var recomendedRecipe: RecipeModel?

    required init?(coder: NSCoder) {
        presenter = HomePresenter()
        super.init(coder: coder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        presenter.setView(view: self)
        presenter.didLoad()
        setupCollection()
        bindPresenterRx()
    }

}

private extension HomeViewController {
    func setupUI() {
        addNavigationbar()
    }
    
    func bindPresenterRx() {
        presenter.recomendedRecipeUrl
            .drive(onNext: { [weak self] recomendedRecipe in
                self?.recomendedRecipe = recomendedRecipe
                self?.recomendedRecipeImage.sd_setImage(with: URL(string: recomendedRecipe?.picture ?? ""), placeholderImage: UIImage(named: ""))
            })
            .disposed(by: disposeBag)
    }
    
    func setupCollection() {
        let adapter = ListAdapter(updater: ListAdapterUpdater(), viewController: self)
        let dataSource = CollectionDataSource(delegate: presenter)
        adapter.then {
            $0.collectionView = collectionView
            $0.dataSource = dataSource
        }

        dataSource
            .updateDriver
            .drive(onNext: {
                adapter.performUpdates(animated: true, completion: nil)
            })
            .disposed(by: disposeBag)
        self.dataSource = dataSource
    }
    
    func addNavigationbar() {
        let navigationItem = UINavigationItem(title: "")
        self.searchController = searchControllerWith(searchResultsController: nil)
        navigationItem.titleView = self.searchController.searchBar

        navigationController?.navigationBar.setItems([navigationItem], animated: false)
        self.definesPresentationContext = false

        searchController.searchBar.rx.cancelButtonClicked
            .subscribe(onNext: { () in
                self.searchTextSubject.onNext("")
            })
            .disposed(by: self.disposeBag)
        
        searchController.searchBar.rx.text.asObservable()
        .map { _ in return self.searchController.searchBar.text ?? "" }
            .distinctUntilChanged()
            .subscribe(onNext: { (text) in
                if !text.isEmpty {
                    self.searchTextSubject.onNext(text)
                }
            })
            .disposed(by: self.disposeBag)
    }
    
    func searchControllerWith(searchResultsController: UIViewController?) -> UISearchController {
        let searchController = UISearchController(searchResultsController: searchResultsController)
        searchController.hidesNavigationBarDuringPresentation = false

        return searchController
     }
}

extension HomeViewController: HomeViewControllerProtocolInput {
    var searchTextDriver: Driver<String> {
        return searchTextSubject.asDriver(onErrorJustReturn: "")
    }
    
    func presentWithID(recipeID: String) {
        if let viewController = RecipeDetailViewController.fromStoryboard("Main") {
            viewController.recipeIDValue = recipeID
            present(viewController, animated: true, completion: nil)
        }
    }
    
    func showError(message: String) {
        let controller = UIAlertController(title: "An error occurred", message: message, preferredStyle: .alert)
        controller.addAction(UIAlertAction(title: "Close", style: .cancel, handler: nil))
        present(controller, animated: true, completion: nil)
    }
}
