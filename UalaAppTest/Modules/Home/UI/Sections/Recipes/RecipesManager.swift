//
//  RecipesManager.swift
//  UalaAppTest
//
//  Created by Christian Correa on 15/11/20.
//

import UIKit
import RxSwift
import RxCocoa
import IGListKit

protocol RecipesManagerDelegate: RecipesSectionControllerDelegate {}

class RecipesManager {

    private weak var delegate: RecipesManagerDelegate?
    private let refreshSubject = PublishSubject<Void>()
    var items: [RecipeModel] = [] {
        didSet {
            refreshSubject.onNext(())
        }
    }
        
    lazy var sectionController: RecipesSectionController = {
        return RecipesSectionController(delegate: self)
    }()
    
    init(delegate: RecipesManagerDelegate) {
        self.delegate = delegate
    }
}

extension RecipesManager: CollectionDataSourceManager {
    
    var diffables: Observable<[ListDiffable]> {
        return refreshSubject.map { [weak self] _ in
            guard let self = self else { return [ListDiffable]() }
            return [RecipesDiffable(items: self.items)]
        }
    }
    
    var reload: Observable<Bool> {
        return Observable<Bool>.never()
    }
    
    func sectionController(for object: ListDiffable) -> ListSectionController? {
        return object is RecipesDiffable ? sectionController : nil
    }
}

extension RecipesManager: RecipesSectionControllerDelegate {
    func didSelect(item: RecipeModel) {
        delegate?.didSelect(item: item)
    }
}
