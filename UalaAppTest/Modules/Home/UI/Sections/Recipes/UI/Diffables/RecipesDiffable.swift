//
//  HomePromotionsDiffable.swift
//  UalaAppTest
//
//  Created by Christian Correa on 15/11/20.
//

import UIKit
import IGListKit

class RecipesDiffable: ListDiffable {
    
    let items: [RecipeModel]
    
    init(items: [RecipeModel]) {
        self.items = items
    }
    
    func diffIdentifier() -> NSObjectProtocol {
        let classIdentifier = String(describing: RecipesDiffable.self)
        let itemsIdentifier = items.compactMap { "\($0.title)_\($0.category)" }.joined(separator: "")
        return "\(classIdentifier)_\(itemsIdentifier)" as NSString
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        return object is RecipesDiffable
    }
}
