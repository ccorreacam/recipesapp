//
//  RecipesSectionController.swift
//  UalaAppTest
//
//  Created by Christian Correa on 15/11/20.
//

import UIKit
import IGListKit

protocol RecipesSectionControllerDelegate: AnyObject {
    func didSelect(item: RecipeModel)
}

final class RecipesSectionController: ListSectionController {
    private weak var delegate: RecipesSectionControllerDelegate?
    private var item: RecipesDiffable?
    
    var currentSelectedIndex: Int? = nil
    
    init(delegate: RecipesSectionControllerDelegate) {
        self.delegate = delegate
    }
    
    private var itemsIsEmpty: Bool {
        return item?.items.isEmpty ?? true
    }
    
    override var inset: UIEdgeInsets {
        get {
            if isLastSection {
                return UIEdgeInsets(top: 0, left: 0, bottom: 40, right: 0)
            }
            return .zero
        }
        set {}
    }
    
    override func sizeForItem(at index: Int) -> CGSize {
        guard let context = collectionContext,
            let _ = item else {
            return CGSize.zero
        }
        return CGSize(width: context.containerSize.width, height: 230)
    }
    
    override func numberOfItems() -> Int {
        return item?.items.count ?? 0
    }
    
    override func cellForItem(at index: Int) -> UICollectionViewCell {
        let cell: RecipeItemCell = reuse(at: index, bundle: Bundle(for: type(of: self)))
        if let item = item?.items[index] {
            cell.setup(recipe: item)
        }
        return cell
    }
    
    override func didUpdate(to object: Any) {
        item = object as? RecipesDiffable
    }
    
    override func didSelectItem(at index: Int) {
        guard let item = item?.items[index] else {
            return
        }
        currentSelectedIndex = index
        delegate?.didSelect(item: item)
    }
}
