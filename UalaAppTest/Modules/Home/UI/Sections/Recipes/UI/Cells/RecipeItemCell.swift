//
//  CouponItemCell.swift
//  UalaAppTest
//
//  Created by Christian Correa on 15/11/20.
//

import UIKit
import SDWebImage

class RecipeItemCell: UICollectionViewCell {
    
    
    @IBOutlet weak var recipeContainer: UIView! {
        didSet {
            recipeContainer.setup(roundedCorners: RoundedCorners(radius: 10, corners: .allCorners))
        }
    }
    @IBOutlet private weak var recipeTitle: UILabel! {
        didSet {
            recipeTitle.textColor = .black
            recipeTitle.adjustsFontSizeToFitWidth = true
        }
    }
    @IBOutlet private weak var recipeImage: UIImageView!
    
    @IBOutlet weak var recipeCategoryContainer: UIView! {
        didSet {
            recipeCategoryContainer.setup(roundedCorners: RoundedCorners(radius: 10, corners: .bottomRight))
            recipeCategoryContainer.backgroundColor = .black
        }
    }
    @IBOutlet private weak var recipeCategory: UILabel! {
        didSet {
            recipeCategory.textColor = .white
            recipeCategory.adjustsFontSizeToFitWidth = true
        }
    }
    
    func setup(recipe: RecipeModel) {
        recipeTitle.text = recipe.title
        recipeCategory.text = recipe.category
        recipeImage.sd_setImage(with: URL(string: recipe.picture), placeholderImage: UIImage(named: ""))
    }
}
