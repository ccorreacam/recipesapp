//
//  RecipeDetailViewController.swift
//  UalaAppTest
//
//  Created by Mobile Dev on 15/11/20.
//

import UIKit
import RxSwift
import RxCocoa
import SDWebImage

protocol RecipeDetailViewControllerProtocolInput {
    var disposeBag: DisposeBag { get }
    var recipeID: Driver<String> { get }
    func showError(message: String)
}

class RecipeDetailViewController: UIViewController {
    
    @IBOutlet private weak var recipeImage: UIImageView!
    @IBOutlet private weak var recipeTitle: UILabel!
    @IBOutlet private weak var recipeInstructions: UITextView!

    var disposeBag = DisposeBag()
    private let presenter: RecipeDetailPresenterProtocol
    private var dataSource: CollectionDataSource?
    private let recipeIDSubject = PublishSubject<String>()
    var recipeIDValue = ""
    
    required init?(coder: NSCoder) {
        presenter = RecipeDetailPresenter()
        super.init(coder: coder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.setView(view: self)
        bindPresenterRx()
    }
}

private extension RecipeDetailViewController {
    func bindPresenterRx() {
        presenter.recipeData
            .drive(onNext: { [weak self] recipe in
                self?.setupUIElements(recipe: recipe)
            })
            .disposed(by: disposeBag)
        
        recipeIDSubject.onNext(recipeIDValue)
    }
    
    func setupUIElements(recipe: RecipeModel?) {
        recipeTitle.text = recipe?.title ?? ""
        recipeInstructions.text = recipe?.completeInstructionsWithIngrediens ?? ""
        recipeImage.sd_setImage(with: URL(string: recipe?.picture ?? ""), placeholderImage: UIImage(named: ""))
    }
}

extension RecipeDetailViewController: RecipeDetailViewControllerProtocolInput {
    var recipeID: Driver<String> {
        return recipeIDSubject.asDriver(onErrorJustReturn: "")
    }
    
    func showError(message: String) {
        let controller = UIAlertController(title: "An error occurred", message: message, preferredStyle: .alert)
        controller.addAction(UIAlertAction(title: "Close", style: .cancel, handler: nil))
        present(controller, animated: true, completion: nil)
    }
}
