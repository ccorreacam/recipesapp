//
//  RecipeDetailPresenter.swift
//  UalaAppTest
//
//  Created by Mobile Dev on 15/11/20.
//

import Foundation
import RxSwift
import RxCocoa

protocol RecipeDetailPresenterProtocol: CollectionDataSourceDelegate {
    var recipeData: Driver<RecipeModel?> { get }
    func setView(view: RecipeDetailViewControllerProtocolInput)
}

class RecipeDetailPresenter {
    
    var dataManager: RecipeDetailDataManagerProtocol?
    var view: RecipeDetailViewControllerProtocolInput?
    var sectionManagers: [CollectionDataSourceManager] = []

    // MARK: Subjects
    private let disposeBag = DisposeBag()
    private let recipeDataSubject = PublishSubject<RecipeModel?>()
    
    init() {
        self.dataManager = RecipeDetailDataManager()
    }
}

extension RecipeDetailPresenter: RecipeDetailPresenterProtocol {
    var recipeData: Driver<RecipeModel?> {
        return recipeDataSubject.asDriver(onErrorJustReturn: nil)
    }

    func setView(view: RecipeDetailViewControllerProtocolInput) {
        self.view = view
        view.recipeID
            .drive(onNext: { [weak self] recipeID in
                self?.requestRecipe(recipeId: recipeID)
            })
            .disposed(by: view.disposeBag)
    }
    
    func showError(message: String) {
        view?.showError(message: message)
    }
}

// MARK: - Privates
private extension RecipeDetailPresenter {
    
    // MARK: Requests
    func requestRecipe(recipeId: String) {
        dataManager?
            .getRecipes(recipeId: recipeId)
            .asObservable()
            .subscribe(onNext: { [weak self] recipes in
                guard let recipe = recipes?.meals?.first else { return }
                self?.recipeDataSubject.onNext(recipe)
            }, onError: { [weak self] error in
                self?.showError(message: error.localizedDescription)
                })
            .disposed(by: disposeBag)
    }
}
