//
//  RecipeDetailDataManager.swift
//  UalaAppTest
//
//  Created by Mobile Dev on 15/11/20.
//


import Foundation
import RxSwift

protocol RecipeDetailDataManagerProtocol {
    func getRecipes(recipeId: String) -> Single<Recipes?>
}

class RecipeDetailDataManager: BaseNetworking {}

extension RecipeDetailDataManager: RecipeDetailDataManagerProtocol {
    func getRecipes(recipeId: String) -> Single<Recipes?> {
        return Single.create { [weak self] single in
            let disposables = Disposables.create()
            guard let self = self else {
                return disposables
            }
            let url = "lookup.php?i=\(recipeId)"
            
            self.executeRequest(url: url, model: Recipes.self) { (data, error) in
                if let data = data {
                    single(.success(data))
                } else if let error = error {
                    single(.error(error))
                }
            }
            return disposables
        }
    }
}
